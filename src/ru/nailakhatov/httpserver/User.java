package ru.nailakhatov.httpserver;

import ru.nailakhatov.httpserver.security.HashGenerator;

import java.util.UUID;

public class User {
    private String name;
    private String password;
    private String token;

    public User (String name, String password, boolean isHash){
        this.name = name;
        if (isHash)
            this.password = password;
        else
            setPassword(password);
    }

    public String generateToken (){
        token = HashGenerator.MD5(UUID.randomUUID().toString());
        return token;
    }

    public void setName (String name){
        this.name = name;
    }

    public void setPassword (String password){
        this.password = HashGenerator.MD5(password);
    }

    public String getToken (){
        return token;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public boolean validatePassword (String password){
        if (this.password.equals(HashGenerator.MD5(password)))
            return true;
        return false;
    }

    @Override
    public String toString (){
        return name;
    }
}
