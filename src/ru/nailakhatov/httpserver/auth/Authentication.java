package ru.nailakhatov.httpserver.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.nailakhatov.httpserver.User;
import ru.nailakhatov.httpserver.logging.LogManager;
import ru.nailakhatov.httpserver.database.DatabseConnection;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;

@Path("auth")
public class Authentication {
    private static LogManager log = new LogManager(Authentication.class);
    private static HashSet<User> loggedUsers = new HashSet<>();
    private static HashSet<User> registeredUsers = new HashSet<>();
    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        registeredUsers.addAll(DatabseConnection.getUsers().stream().collect(Collectors.toList()));
    }

    @POST
    @Path("register")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response register(@FormParam("user") String name,
                             @FormParam("password") String password) {

        if (name == null || password == null) {
            log.info("Bad request");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        if (!registerUser(name, password)) {
            log.info(String.format("User %s is already registered.", name));
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }

        log.info(String.format("User %s is registered.", name));
        return Response.ok(String.format("User %s is registered.", name)).build();
    }

    private boolean registerUser(String name, String password) {
        for (User u : registeredUsers)
            if (u.getName().equalsIgnoreCase(name))
                return false;
        User user = new User(name, password, false);
        DatabseConnection.registerUser(user);
        registeredUsers.add(user);
        return true;
    }

    private boolean authorizeUser(User user) {
        for (User u : loggedUsers)
            if (u.getName().equalsIgnoreCase(user.getName()))
                return false;
        loggedUsers.add(user);
        return true;
    }

    private User getRegisteredUser(String name) {
        for (User u : registeredUsers)
            if (u.getName().equalsIgnoreCase(name))
                return u;
        return null;
    }

    private boolean logout (User user) {
        for (User u : loggedUsers){
            if (u.getName().equalsIgnoreCase(user.getName())){
                loggedUsers.remove(u);
                return true;
            }
        }
        return false;
    }

    @POST
    @Path("login")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response login(@FormParam("user") String name,
                          @FormParam("password") String password) {

        if (name == null || password == null) {
            log.info("Bad request");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        User user = getRegisteredUser(name);
        if (user == null) {
            log.info(String.format("User %s is not registered.", name));
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        if (!user.validatePassword(password)){
            log.info("IllegalPassword");
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        if (!authorizeUser(user)) {
            log.info(String.format("User %s is already logeed in.", name));
            return Response.ok(getRegisteredUser(name).getToken()).build();
        }

        String token = user.generateToken();
        log.info(String.format("User %s is logged in with token %s.", name, token));
        return Response.ok(token).build();
    }

    @Authorized
    @POST
    @Path("logout")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response logout(@HeaderParam("Authorization") String token) {
        token = getTokenByHeader(token);
        User user = getUserByToken(token);
        if (!logout(user)){
            log.info(String.format("User %s is not logged in."));
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        log.info(String.format("User %s is logged out", user.getName()));
        return Response.ok("Logged out").build();
    }

    public static boolean validateToken (String token) throws Exception{
        for (User u : loggedUsers)
            if (u.getToken().equals(token)) {
                log.info(String.format("Correct token from %s.", u.getName()));
                return true;
            }
        log.info(String.format("Illegal Token"));
        throw new Exception("Illegal Token");
    }

    public static String getOnline (){
        String result = "";
        ArrayList<User> temp = loggedUsers.stream().collect(Collectors.toCollection(ArrayList::new));
        ArrayList<String> tmp = new ArrayList<>();
        for (User u : temp)
            tmp.add(u.getName());
        try {
            result = mapper.writeValueAsString(tmp);
            result = String.format("{users: %s}", result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (result.equals("") || loggedUsers.size() == 0)
            result = "{\"users\": \"No users online\"}";

        return result;
    }

    public static User getUserByToken (String token){
        for (User u: loggedUsers)
            if (u.getToken().equals(token))
                return u;
        return null;
    }

    public static String getTokenByHeader (String header){
        header = header.substring("Bearer".length()).trim();
        return header;
    }
}
