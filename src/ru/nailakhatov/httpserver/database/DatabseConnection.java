package ru.nailakhatov.httpserver.database;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import ru.nailakhatov.httpserver.User;
import ru.nailakhatov.httpserver.logging.LogManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class DatabseConnection {

    private static LogManager log = new LogManager(DatabseConnection.class);

    private static Connection db;

    private static String base;
    private static String user;
    private static String password;

    public static void initDB (String base, String username, String password){
        DatabseConnection.base = base;
        DatabseConnection.user = username;
        DatabseConnection.password = password;

        try {
            if (base.contains("sqlite")) {
                Class.forName("org.sqlite.JDBC");
            } else if (base.contains("mysql")) {
                Class.forName("com.mysql.jdbc.Driver");
            }

            Properties properties = new Properties();
            properties.setProperty("user", user);
            properties.setProperty("password", password);
            properties.setProperty("useUnicode", "true");
            properties.setProperty("characterEncoding", "UTF-8");
            db = DriverManager.getConnection(base, properties);

            db.createStatement().execute("CREATE TABLE IF NOT EXISTS `atom_users`" +
                    " (`username` varchar(16) NOT NULL UNIQUE, " +
                    "`password` text NOT NULL);");
            log.info("Database initialized.");
        } catch (Exception e){
            log.error(e);
        }
    }

    public static User getUser (String name){
        checkConnection();
        User user = null;
        try {
            PreparedStatement ps = db.prepareStatement("SELECT `password` FROM `atom_users` WHERE `name` = ?");
            ps.setString(1, name.toLowerCase());
            ResultSet res = ps.executeQuery();

            if (res.next()){
                user = new User(name, res.getString("password"), true);
                res.close();
                ps.close();
            }
        } catch (Exception e){
            log.error(e);
        }

        return user;
    }

    public static ArrayList<User> getUsers (){
        ArrayList<User> users = new ArrayList<>();
        try {
            PreparedStatement ps = db.prepareStatement("SELECT * FROM `atom_users`");
            ResultSet res = ps.executeQuery();

            while (res.next()){
                users.add(new User(res.getString("username"),
                        res.getString("password"),
                        true));
            }
        } catch (Exception e){
            log.error(e);
        }
        return users;
    }

    public static boolean registerUser (User user){
        checkConnection();
        try {
            PreparedStatement ps = db.prepareStatement("INSERT INTO `atom_users` (`username`, `password`) VALUES (?, ?)");
            ps.setString(1, user.getName());
            ps.setString(2, user.getPassword());
            ps.execute();
            ps.close();

            return true;
        } catch (Exception e){
            log.error(e);
        }

        return false;
    }

    public static boolean rename (User user, String name){
        checkConnection();
        try {
            PreparedStatement ps = db.prepareStatement("DELETE FROM `atom_users` WHERE `username` = ?");
            ps.setString(1, user.getName());
            ps.execute();
            ps.close();
            ps = db.prepareStatement("INSERT INTO `atom_users` (`username`, `password`) VALUES (?, ?)");
            ps.setString(1, name);
            ps.setString(2, user.getPassword());
            ps.execute();
            ps.close();

            return true;
        } catch (Exception e){
            log.error(e);
        }

        return false;
    }

    private static void checkConnection (){
        try {
            if (db.isClosed()) {
                initDB(base, user, password);
            }
        } catch (SQLException e) {
            log.error(e);
        }

        //Для воостановления соединения в случае длительного неиспользования соединения с БД
        try {
            PreparedStatement ps = db.prepareStatement("SELECT 1");
            ps.execute();
            ps.close();
        } catch (CommunicationsException e){
            log.error("Connection is restored.");
        } catch (SQLException e){
            log.error(e);
        }
    }
}
