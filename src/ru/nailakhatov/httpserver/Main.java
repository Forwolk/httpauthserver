package ru.nailakhatov.httpserver;

import ru.nailakhatov.httpserver.api.ApiServlet;
import ru.nailakhatov.httpserver.database.DatabseConnection;
import ru.nailakhatov.httpserver.logging.LogManager;

public class Main {
    private static String db_user;
    private static String db_password;
    private static String db_host;

    private static LogManager log = new LogManager(Main.class);

    public static void main (String[] args) throws Exception{
        int port = 12580;
        if (args.length == 4){
            db_host = args[0];
            db_user = args[1];
            db_password = args[2];
            try {
                port = Integer.valueOf(args[3]);
            } catch (NumberFormatException e){
                port = 12580;
            }
        } else if (args.length == 3){
            db_host = args[0];
            db_user = args[1];
            db_password = args[2];
        } else if (args.length > 4 || (args.length < 3 && args.length > 0)){
            log.error("Illegal number of arguments.");
        } else {
            db_host = "jdbc:mysql://localhost/database";
            db_user = "root";
            db_password = "";
        }
        log.info("Trying to initializate database...");
        DatabseConnection.initDB(db_host, db_user, db_password);
        ApiServlet.start(port);
    }
}
