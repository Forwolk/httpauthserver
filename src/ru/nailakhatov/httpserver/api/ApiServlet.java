package ru.nailakhatov.httpserver.api;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ru.nailakhatov.httpserver.auth.AuthenticationFilter;
import ru.nailakhatov.httpserver.logging.LogManager;

public class ApiServlet {
    private static int port;
    private static LogManager log = new LogManager(ApiServlet.class);

    public static void start (int p) throws Exception{
        ApiServlet.port = p;
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        Server jettyServer = new Server(port);
        jettyServer.setHandler(context);

        ServletHolder jerseyServlet = context.addServlet(
                org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);

        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.packages",
                "ru.nailakhatov.httpserver"
        );

        jerseyServlet.setInitParameter(
                "com.sun.jersey.spi.container.ContainerRequestFilters",
                AuthenticationFilter.class.getCanonicalName()
        );

        try {
            jettyServer.start();
            String host = jettyServer.getURI().getHost();
            log.info(String.format("Server {ip: %s} is running on port %s.", host, port));
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
    }

    private ApiServlet() {
    }
}
