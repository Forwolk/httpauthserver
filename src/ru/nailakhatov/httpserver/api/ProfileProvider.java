package ru.nailakhatov.httpserver.api;

import ru.nailakhatov.httpserver.User;
import ru.nailakhatov.httpserver.auth.Authentication;
import ru.nailakhatov.httpserver.auth.Authorized;
import ru.nailakhatov.httpserver.database.DatabseConnection;
import ru.nailakhatov.httpserver.logging.LogManager;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("profile")
public class ProfileProvider {
    private static LogManager log = new LogManager(ProfileProvider.class);

    @Authorized
    @POST
    @Path("name")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response register(@FormParam("name") String name,
                             @HeaderParam("Authorization") String token) {
        token = Authentication.getTokenByHeader(token);
        User user = Authentication.getUserByToken(token);
        if (name == null || name.equals(""))
            return Response.status(Response.Status.BAD_REQUEST).build();
        if (user != null) {
            log.info(String.format("Changing name %s -> %s", user.getName(), name));
            if (DatabseConnection.rename(user, name))
                user.setName(name);
            else
                return Response.status(Response.Status.NOT_ACCEPTABLE).build();
            return Response.ok(String.format("Your name changed to %s", name)).build();
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
}
