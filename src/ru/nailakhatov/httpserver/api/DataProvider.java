package ru.nailakhatov.httpserver.api;

import ru.nailakhatov.httpserver.auth.Authentication;
import ru.nailakhatov.httpserver.logging.LogManager;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("data")
public class DataProvider {
    private static LogManager log = new LogManager(DataProvider.class);

    @GET
    @Path("users")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response online (){
        String result = Authentication.getOnline();
        log.info(String.format("Online users request"));
        return Response.ok(result).build();
    }
}
