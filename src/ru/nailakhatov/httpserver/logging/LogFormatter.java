package ru.nailakhatov.httpserver.logging;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

public class LogFormatter extends SimpleFormatter {
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final Date dat = new Date();

    public synchronized String format(LogRecord record) {
        dat.setTime(record.getMillis());
        String dateTime = SDF.format(Calendar.getInstance().getTime());
        String message = formatMessage(record);
        return String.format("[%s] %s\n", dateTime, message);
    }
}
